# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* ButterFlyAI.Demo
* Version: 1.0
* Formal requirement for the employee selection process

### How do I get set up? ###

* Clone the repo to the folder in local machine.
* Installing NodeJs is required to run this project.
* Open the terminal of choice and from the cloned folder run command - npm install
* Confgiurations:
    * Make sure the .env file is available in the project root
    * You can change the port on which the web server should run. Default is PORT=8080
    * Set the company name of your choice as COMPANY=My Test Company and save .env.
* Go to terminal again and run the command - npm start
* Check the console output for any errors. If every thing is fine, It would display as Express loaded and listening on the port you configured.
* Open the browser of your choice and try to navigate to http://localhost:8080/ (8080 being the default, this should be the port you have configured)

### Who do I talk to? ###

* Jayan Udayakantha
* jayanudayakantha@gmail.com
* Skype: jayan.cms.lk