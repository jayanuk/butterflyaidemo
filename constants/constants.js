module.exports = {
    DEFAULT_COMPANY:  "Global Inc. ",
    HTTP_STATUS: {
        REDIRECT: 302,
        SERVER_ERROR: 500
    },
    PAGE_INFO: {
        TITLE: "Butterfly-Demo",
        HOME: "home"
    },
    PAGE_VIEW: {
        HOME: "index",
        QUESTIONS: "questions",
        THANKS: "thanks"
    }
};