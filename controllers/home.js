import * as config from "../config";
import * as constants from "../constants";

module.exports.getView = async (req, res) => {
    try {
        res.render(constants.PAGE_VIEW.HOME, {
            title: constants.PAGE_INFO.TITLE,
            page: constants.PAGE_INFO.HOME,
            company: `${config.company} ` || constants.DEFAULT_COMPANY,
        });
    } catch (e) {
        res.render("error", { error: { message: e, status: constants.HTTP_STATUS.SERVER_ERROR } });
    }
};
