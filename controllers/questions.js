import * as config from "../config";
import * as constants from "../constants";
import * as services from "../services";

module.exports.getView = async (req, res) => {
    try {
        const id = req.params.id || 0;
        const questions = await services.questions.getQuestions();
        const header = await services.headers.getHeader(id);
        res.render(constants.PAGE_VIEW.QUESTIONS, {
            title: constants.PAGE_INFO.TITLE,
            questions,
            header,
            company: `${config.company} ` || constants.DEFAULT_COMPANY,
        });
    } catch (e) {
        res.render("error", { error: { message: e, status: constants.HTTP_STATUS.SERVER_ERROR } });
    }
};

module.exports.add = async (req, res) => {
    try {
        res.redirect(constants.HTTP_STATUS.REDIRECT, "/thank-you");
    } catch (e) {
        res.render("error", { error: { message: e, status: constants.HTTP_STATUS.SERVER_ERROR } });
    }
};
