module.exports.questions = [
    {
        id: 1,
        topic: "I feel like I have a healthy work/life balance.",
        area: "Work/Life Balance",
    },
    {
        id: 2,
        topic: "I am satisfied with my roles and responsibilities.",
        area: "Roles and Responsibilities",
    },
    {
        id: 3,
        topic: "I like my work environment, and I believe it helps me perform at my best.",
        area: "Workplace",
    },
    {
        id: 4,
        topic: "I feel comfortable working and interacting with the colleagues on my team.",
        area: "Teamwork",
    },
    {
        id: 5,
        topic: "My direct manager gives me necessary support and clear objectives.",
        area: "Management",
    },
];

module.exports.headers = [
    {
        id: 1,
        header: "Oops, something needs to change. Thank you for your Feedback.",
        image: "VeryUnhappy.svg",
    },
    {
        id: 2,
        header: "Mmmmh, things should improve. Thank you for your Feedback.",
        image: "Unhappy.svg",
    },
    {
        id: 3,
        header: "OK… things could be better. Thank you for your Feedback.",
        image: "Neutral.svg",
    },
    {
        id: 4,
        header: "Great! Thank you for your Feedback.",
        image: "Happy.svg",
    },
    {
        id: 5,
        header: "Awesome! Thank you for your Feedback.",
        image: "VeryHappy.svg",
    },
];
