import { Router} from "express";
import views from "./views";

export default () => {
    const router = Router();
    views(router);
    return router;
}
