import * as controllers from "../controllers";

export default (router) => {
    router.get("/", controllers.home.getView);
    router.get("/demo-question/:id?", controllers.questions.getView);
    router.post("/demo-question/add", controllers.questions.add);
    router.get("/thank-you", controllers.thankyou.getView);
};
