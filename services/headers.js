import * as data from "../data";

module.exports.getHeader = async(id) => {
    let header = data.headers.find((item) => item.id == id);
    if(!header)
        throw new Error("Definitely out of scope, No header for this page");
    return header;
};
