import * as data from "../data";
import * as helpers from "../helpers";

module.exports.getQuestions = async(id) => {    
    return helpers.shuffleArray(data.questions);
};